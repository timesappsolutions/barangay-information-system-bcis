<?php

use Illuminate\Database\Seeder;

class AdminAccountSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'image' => 'default.png',
            'status' => 1
        ]);
    }
}
