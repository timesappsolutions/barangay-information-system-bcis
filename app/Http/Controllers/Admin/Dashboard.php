<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;    
use App\Models\Admin;   
use App\Models\Residents; 
use App\Models\Events as event;  

class Dashboard extends Controller
{
    public function index(){    
        $calendar = array();
        $calendar_data = event::valid();
        foreach ($calendar_data as $k => $v) {
            array_push($calendar, array(
                "title"     => $v->title,
                "start"     => $v->date,
                "className" => "fc-event--orange",
                "allDay"    => "!0"
            ));
        }

        $resData = Residents::groupBy('purok')->whereIn('status',[1])->get();
        $purok   = array();
        $counts  = array();
        foreach ($resData as $k => $v) {
            array_push($purok, "Purok #".$v->purok." as of ".date('Y'));
            array_push($counts, Residents::whereIn('purok',[$v->purok])->whereIn('status',[1])->count());
        }
    	return view('admin.dashboard.index')
            ->with([
                "data" => array(
                    "residents"    => Residents::whereIn('status',[1])->count(),
                    "households"   => Residents::whereIn('status',[1])->whereIn('position',['Head'])->count(),
                    "senior"       => Residents::whereIn('status',[1])->whereIn('position',['Head'])->count(),
                ),
                "graph" => array(
                    "purok"   => $purok,
                    "counts"  => $counts
                ),
                "events" => json_encode($calendar),
                "active" => 'dashboard',
                "title"  => 'Dashboard'
            ]); 
    } 
}
