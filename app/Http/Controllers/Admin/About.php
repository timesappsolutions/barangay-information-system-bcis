<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Terms as terms;  
use App\Models\Officials as official;

define('location','admin.about.');
define('home','admin.about');

class About extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"     => Terms::valid(),
                "official" => array(
                	"captain"   => official::whereIn('position',['Captain'])->whereIn('status',[1])->first(),
                	"secretary" => official::whereIn('position',['Secretary'])->whereIn('status',[1])->first(),
                	"treasurer" => official::whereIn('position',['Treasurer'])->whereIn('status',[1])->first(),
                	"sub"       => official::whereNotIn('position',['Treasurer','Captain','Secretary'])->whereIn('status',[1])->get()
                ),
                "active"    => 'about',
                "title"     => 'About'
            ]); 

    }  
}
