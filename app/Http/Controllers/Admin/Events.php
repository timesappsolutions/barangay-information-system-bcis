<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Events as event;  

define('location','admin.events.');
define('home','admin.events');

class Events extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"   => event::valid(),
                "active" => 'events',
                "title"  => 'Events'
            ]); 

    } 
    public function add(Request $request){ 

        try{
            // if(Input::file('logo')){ 

            //     $file = Input::file('logo');  
            //     $fileName = md5(str_random(15)) . '_' . $file->getClientOriginalName(); 
            //     $file->move('public/uploads/banks/' , $fileName); 
            //     $val['image2'] = $fileName; 

            // } else {

            //     $fileName = 'default.png';

            // }

            $data               = new event;
            $data->title        = $request->input('title');
            $data->date         = $request->input('date'); 
            $data->remarks      = $request->input('remarks');   
            $data->status       = '1';

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Added !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = event::find($id);
            $data->title        = $request->input('title');
            $data->date         = $request->input('date'); 
            $data->remarks      = $request->input('remarks');   

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = event::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
}
