<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;    
use App\Models\Admin; 

class Authentication extends Controller
{
    public function index(){   
    	return view('admin.login.index')
            ->with([
                'title' => 'Administrator Login'
            ]);
    }
    public function auth(Request $request){ 

    	$credentials = array(
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'status'   => 1
        );

    	if(Auth::attempt($credentials)){

    		$details = Admin::find(Auth::id());

        	$request->session()->put('id',    $details->id); 

			return redirect()
			       ->route('admin.dashboard')
			       ->with('success',  'Welcome Admin');  

    	} else {

			return redirect()
			       ->route('admin.login')
			       ->with('error',  'Invalid Login Input !');  

    	}	 
    }
    public function logout(Request $request){ 

    	$request->session()->flush();
 
    	return redirect()
    	       ->route('admin.login');
    }
}
