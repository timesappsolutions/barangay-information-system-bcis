<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;    
use App\Models\Officials as official;  
use App\Models\Residents as recidents;   

define('location','admin.officials.');
define('home','admin.officials');

class Officials extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"        => official::valid(), 
                "recidents"   => recidents::valid(),
                "active"      => 'officials',
                "title"       => 'Officials'
            ]); 

    } 
    public function add(Request $request){ 

        // try{ 

            $data               = new official; 
            $data->resident_id  = $request->input('resident_id'); 
            $data->position     = $request->input('position');    
            $data->status       = '1';

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Added !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        // } catch (\Illuminate\Database\QueryException $e){ 
        //     return redirect()->route(home)
        //         ->with([
        //             "error"   => "Somethin wen't wrong !"
        //         ]); 
        // }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = official::find($id);
            $data->resident_id  = $request->input('resident_id'); 
            $data->position     = $request->input('position');    
            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = official::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
}
