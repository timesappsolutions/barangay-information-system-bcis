<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Residents as resident; 
use App\Models\Officials as official;  
use App\Models\Events as event;  
use App\Models\Ordinances as ordinance;  
use App\Models\Offenses as offense;  
use App\Models\Incidents as incident;  

define('sub','admin.print.'); 
define('location', sub.'index'); 

class Reports extends Controller
{    
    public function hr_print(Request $request){   
        $date = array(
            date($request->post('year').'-01-01' . ' 00:00:00', time()),
            date($request->post('year').'-12-29' . ' 24:60:60', time())
        ); 
        $data = array(
            "residents" => array(
                "title"   => 'residents',
                "headers" => ['fullname','position','house_no','purok','birthplace','birthday','sex','civil_status','citizenship','occupation'],
                "count"   => resident::whereIn('status',[1])->whereBetween('created_at',$date)->count(),
                "data"    => resident::whereIn('status',[1])->whereBetween('created_at',$date)->get()
            ),
            "indigent" => array(
                "title"   => 'indigent',
                "headers"  => ['fullname','mname','lname','position','house_no','purok','birthplace','birthday','sex','civil_status','citizenship','occupation'],
                "count"   => resident::whereIn('status',[1])->whereIn('indigency',[1])->whereBetween('created_at',$date)->count(),
                "data"    => resident::whereIn('status',[1])->whereIn('indigency',[1])->whereBetween('created_at',$date)->get()
            ),
            "events" => array(
                "title"   => 'events',
                "headers" => ['title','date','remarks','created_at','updated_at'],
                "count"   => event::whereIn('status',[1])->whereBetween('created_at',$date)->count(),
                "data"    => event::whereIn('status',[1])->whereBetween('created_at',$date)->get()
            ),
            "ordinances" => array(
                "title"   => 'ordinances',
                "headers" => ['no','title','summary','approved_by','created_at','updated_at'],
                "count"   => ordinance::whereIn('status',[1])->whereBetween('created_at',$date)->count(),
                "data"    => ordinance::whereIn('status',[1])->whereBetween('created_at',$date)->get()
            ),
            "incidents" => array(
                "title"   => 'incidents',
                "headers" => ['type','location','summary','date_happen','processed_by'],
                "count"   => incident::whereIn('status',[1])->whereBetween('created_at',$date)->count(),
                "data"    => incident::whereIn('status',[1])->whereBetween('created_at',$date)->get()
            ),
            "offences" => array(
                "title"   => 'offences',
                "headers" => ['ordinance #','resident #','location','remarks','processed_by'],
                "count"   => offense::whereIn('status',[1])->whereBetween('created_at',$date)->count(),
                "data"    => offense::whereIn('status',[1])->whereBetween('created_at',$date)->get()
            ),
        );

        return view(sub.'hr_print')
            ->with([  
                "generate" => $request->generate,
                "data"   => $data,
                "date"   => $date,
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'xczxc',
                "title"  => 'Historical Records'
            ]);  
    }
    public function hr(){   
        return view(sub.'hr')
            ->with([  
                "active" => 'indigent',
                "title"  => 'Indigent List'
            ]);  
    }
    public function residents(){     
        return view(location)
            ->with([ 
                "headers"=> ['fname','mname','lname','position','house_no','purok','birthplace','birthday','sex','civil_status','citizenship','occupation'],
                "data"   => resident::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'residents',
                "title"  => 'Residents List'
            ]);  
    } 
    public function indigent(){     
        return view(location)
            ->with([ 
                "headers"=> ['fname','mname','lname','position','house_no','purok','birthplace','birthday','sex','civil_status','citizenship','occupation'],
                "data"   => resident::whereIn('status',[1])->whereIn('indigency',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'indigent',
                "title"  => 'Indigent List'
            ]);  
    } 
    public function events(){     
        return view(location)
            ->with([ 
                "headers"=> ['title','date','remarks','created_at','updated_at'],
                "data"   => event::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'events',
                "title"  => 'Events List'
            ]);  
    } 
    public function ordinances(){     
        return view(location)
            ->with([ 
                "headers"=> ['no','title','summary','approved_by','created_at','updated_at'],
                "data"   => ordinance::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'ordinances',
                "title"  => 'Ordinances List'
            ]);  
    } 
    public function incidents(){     
        return view(location)
            ->with([ 
                "headers"=> ['type','location','summary','date_happen','processed_by'],
                "data"   => incident::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'incidents',
                "title"  => 'Incidents List'
            ]);  
    } 
    public function offences(){     
        return view(location)
            ->with([ 
                "headers"=> false,
                "table"  => 'offenses',
                "data"   => offense::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'offences',
                "title"  => 'Offences List'
            ]);  
    } 
    public function officials(){     
        return view(location)
            ->with([ 
                "headers"=> false,
                "table"  => 'officials',
                "data"   => official::whereIn('status',[1])->get(),
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'officials',
                "title"  => 'Officials List'
            ]);  
    }   
}
 