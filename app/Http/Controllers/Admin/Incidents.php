<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Incidents as incident;  
use App\Models\Officials as official;  

define('location','admin.incidents.');
define('home','admin.incidents');

class Incidents extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"   => incident::valid(),
                "active" => 'incidents',
                "title"  => 'Incidents'
            ]); 

    } 
    public function add(Request $request){ 

        try{ 

            $data               = new incident;
            $data->type         = $request->input('type');
            $data->location     = $request->input('location'); 
            $data->date_happen  = $request->input('date_happen'); 
            $data->processed_by = $request->input('processed_by'); 
            $data->remarks      = $request->input('summary');  
            $data->status       = '1';

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Added !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){  
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = incident::find($id);
            $data->type         = $request->input('type');
            $data->location     = $request->input('location'); 
            $data->date_happen  = $request->input('date_happen'); 
            $data->processed_by = $request->input('processed_by'); 
            $data->remarks      = $request->input('summary');  

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = incident::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function cert(Request $request,$id){  
        return view(location.'certificate')
            ->with([ 
                "data"   => incident::find($id), 
                "captain"=> official::where('position','=','captain')->first(),
                "active" => 'residents',
                "title"  => 'Residents'
            ]); 

    } 
}
