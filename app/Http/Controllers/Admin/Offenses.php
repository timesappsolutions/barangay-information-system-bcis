<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Offenses as offense; 
use App\Models\Ordinances as ordinance;  
use App\Models\Residents as resident;   

define('location','admin.offenses.');
define('home','admin.offenses');

class Offenses extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"        => offense::valid(),
                "recidents"   => resident::valid(),
                "ordinances"  => ordinance::valid(),
                "active"      => 'offenses',
                "title"       => 'Offenses'
            ]); 

    } 
    public function add(Request $request){ 

        try{ 

            $data               = new offense;
            $data->ordinance_id = $request->input('ordinance_id');
            $data->resident_id  = $request->input('resident_id'); 
            $data->location     = $request->input('location');   
            $data->remarks      = $request->input('remarks');  
            $data->processed_by = $request->input('processed_by');  
            $data->status       = '1';

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Added !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = offense::find($id);
            $data->ordinance_id = $request->input('ordinance_id');
            $data->resident_id  = $request->input('resident_id'); 
            $data->location     = $request->input('location');   
            $data->remarks      = $request->input('remarks');  
            $data->processed_by = $request->input('processed_by');  
            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = offense::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
}
