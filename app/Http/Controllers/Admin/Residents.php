<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Residents as resident; 
use App\Models\Officials as official;  

define('location','admin.residents.');
define('home','admin.residents');

class Residents extends Controller
{    
    public function households(){    

        return view(location.'households')
            ->with([ 
                "data"   => resident::households(),
                "active" => 'households',
                "title"  => 'Households'
            ]); 

    }
    public function index(){    

        return view(location.'index')
            ->with([ 
                "data"   => resident::valid(),
                "countries" => json_decode(file_get_contents('countries.json')),
                "occupation" => json_decode(file_get_contents('occupation.json')),
                "active" => 'residents',
                "title"  => 'Residents'
            ]); 

    } 
    public function add(Request $request){ 

        try{
            // if(Input::file('logo')){ 

            //     $file = Input::file('logo');  
            //     $fileName = md5(str_random(15)) . '_' . $file->getClientOriginalName(); 
            //     $file->move('public/uploads/banks/' , $fileName); 
            //     $val['image2'] = $fileName; 

            // } else {

            //     $fileName = 'default.png';

            // }

            if (resident::where('fname',  'LIKE','%'.$request->input('fname').'%') 
                        ->where('lname','LIKE','%'.$request->input('lname').'%')
                        ->count() != 0) {

                return redirect()->route(home)
                    ->with([
                        "error"   => "Resident already exist !"
                    ]);

            } else {  

                $data               = new resident;
                $data->fname        = ucfirst($request->input('fname'));
                $data->mname        = ucfirst($request->input('mname')); 
                $data->lname        = ucfirst($request->input('lname')); 
                $data->house_no     = $request->input('house_no'); 
                $data->position     = $request->input('position'); 
                $data->purok        = $request->input('purok'); 
                $data->birthplace   = $request->input('birthplace'); 
                $data->birthday     = $request->input('birthday'); 
                $data->sex          = $request->input('sex'); 
                $data->civil_status = $request->input('civil_status'); 
                $data->citizenship  = $request->input('citizenship'); 
                $data->occupation   = $request->input('occupation');   
                $data->status       = '1';

                if ($data->save()) {
                    return redirect()->route(home)
                        ->with([
                            "success" => "Successfully Added !"
                        ]);
                } else {
                    return redirect()->route(home)
                        ->with([
                            "error"   => "Somethin wen't wrong !"
                        ]);
                } 

            }
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = resident::find($id);
            $data->fname        = ucfirst($request->input('fname'));
            $data->mname        = ucfirst($request->input('mname')); 
            $data->lname        = ucfirst($request->input('lname')); 
            $data->house_no     = $request->input('house_no'); 
            $data->position     = $request->input('position'); 
            $data->purok        = $request->input('purok'); 
            $data->birthplace   = $request->input('birthplace'); 
            $data->birthday     = $request->input('birthday'); 
            $data->sex          = $request->input('sex'); 
            $data->civil_status = $request->input('civil_status'); 
            $data->citizenship  = $request->input('citizenship'); 
            $data->occupation   = $request->input('occupation');   

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){  
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = resident::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function cert(Request $request,$id){  
        return view(location.'certificate')
            ->with([ 
                "data"   => resident::find($id),
                "captain"=> official::where('position','=','captain')->whereIn('status',[1])->first(),
                "active" => 'residents',
                "title"  => 'Residents'
            ]); 

    }
    public function indi(Request $request,$id){ 

        $alter            = resident::find($id);
        $alter->indigency = 1;
        $alter->save();

        return view(location.'indigency')
            ->with([ 
                "data"   => resident::find($id),
                "captain"=> official::where('position','=','captain')->whereIn('status',[1])->first(),
                "active" => 'residents',
                "title"  => 'Residents'
            ]); 
   
    }
}
