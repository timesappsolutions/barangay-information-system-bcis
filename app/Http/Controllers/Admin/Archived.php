<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Residents as resident; 
use App\Models\Officials as official;  
use App\Models\Events as event;  
use App\Models\Ordinances as ordinance;  
use App\Models\Offenses as offense;  
use App\Models\Incidents as incident;  

define('location',       'admin.archived.');
define('res_page',       'admin.archived.residents');
define('events_page',    'admin.archived.events');
define('ordinances_page','admin.archived.ordinances');
define('offenses_page',  'admin.archived.offenses');
define('incidents_page', 'admin.archived.incidents');

class Archived extends Controller
{    
    public function residents(){     
        return view(location.'res_page')
            ->with([ 
                "data"   => resident::archived(),
                "active" => 'archived',
                "title"  => 'Archived Residents'
            ]);  
    }  
    public function residents_ret(Request $request,$id){  
        try{  
            $data            = resident::find($id); 
            $data->status    = '1';

            if ($data->save()) {
                return redirect()->route(res_page)
                    ->with([
                        "success" => "Successfully Returned to Database !"
                    ]);
            } else {
                return redirect()->route(res_page)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(res_page)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    } 
    public function events(){     
        return view(location.'events_page')
            ->with([ 
                "data"   => event::archived(),
                "active" => 'archived',
                "title"  => 'Archived Events'
            ]);  
    }  
    public function events_ret(Request $request,$id){  
        try{  
            $data            = event::find($id); 
            $data->status    = '1';

            if ($data->save()) {
                return redirect()->route(events_page)
                    ->with([
                        "success" => "Successfully Returned to Database !"
                    ]);
            } else {
                return redirect()->route(events_page)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(events_page)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    } 
    public function ord(){     
        return view(location.'ordinances_page')
            ->with([ 
                "data"   => ordinance::archived(),
                "active" => 'archived',
                "title"  => 'Archived Ordinances'
            ]);  
    }  
    public function ord_ret(Request $request,$id){  
        try{  
            $data            = ordinance::find($id); 
            $data->status    = '1';

            if ($data->save()) {
                return redirect()->route(ordinances_page)
                    ->with([
                        "success" => "Successfully Returned to Database !"
                    ]);
            } else {
                return redirect()->route(ordinances_page)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(ordinances_page)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    } 
    public function offenses(){     
        return view(location.'offenses_page')
            ->with([ 
                "data"   => offense::archived(),
                "active" => 'archived',
                "title"  => 'Archived Offenses'
            ]);  
    }  
    public function offenses_ret(Request $request,$id){  
        try{  
            $data            = offense::find($id); 
            $data->status    = '1';

            if ($data->save()) {
                return redirect()->route(offenses_page)
                    ->with([
                        "success" => "Successfully Returned to Database !"
                    ]);
            } else {
                return redirect()->route(offenses_page)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(offenses_page)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    } 
    public function incidents(){     
        return view(location.'incidents_page')
            ->with([ 
                "data"   => incident::archived(),
                "active" => 'archived',
                "title"  => 'Archived Incidents'
            ]);  
    }  
    public function incidents_ret(Request $request,$id){  
        try{  
            $data            = incident::find($id); 
            $data->status    = '1';

            if ($data->save()) {
                return redirect()->route(incidents_page)
                    ->with([
                        "success" => "Successfully Returned to Database !"
                    ]);
            } else {
                return redirect()->route(incidents_page)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(incidents_page)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    } 
}
