<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;  
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;   
use App\Models\Ordinances as ordinance;  

define('location','admin.ordinances.');
define('home','admin.ordinances');

class Ordinances extends Controller
{    
    public function index(){    

    	return view(location.'index')
            ->with([ 
                "data"   => ordinance::valid(),
                "active" => 'ordinances',
                "title"  => 'Ordinances'
            ]); 

    } 
    public function add(Request $request){ 

        try{
            // if(Input::file('logo')){ 

            //     $file = Input::file('logo');  
            //     $fileName = md5(str_random(15)) . '_' . $file->getClientOriginalName(); 
            //     $file->move('public/uploads/banks/' , $fileName); 
            //     $val['image2'] = $fileName; 

            // } else {

            //     $fileName = 'default.png';

            // }

            $data               = new ordinance;
            $data->no           = $request->input('no');
            $data->title        = $request->input('title'); 
            $data->summary      = $request->input('summary');   
            $data->approved     = $request->input('approved');  
            $data->status       = '1';

            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Added !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function update(Request $request,$id){ 

        try{
              
            $data               = ordinance::find($id);
            $data->no           = $request->input('no');
            $data->title        = $request->input('title'); 
            $data->summary      = $request->input('summary');   
            $data->approved     = $request->input('approved');  
            if ($data->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Updated !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
    public function delete(Request $request,$id){ 

        try{ 

            $banks            = ordinance::find($id); 
            $banks->status    = '0';

            if ($banks->save()) {
                return redirect()->route(home)
                    ->with([
                        "success" => "Successfully Deleted !"
                    ]);
            } else {
                return redirect()->route(home)
                    ->with([
                        "error"   => "Somethin wen't wrong !"
                    ]);
            } 
        } catch (\Illuminate\Database\QueryException $e){ 
            return redirect()->route(home)
                ->with([
                    "error"   => "Somethin wen't wrong !"
                ]); 
        }
    }
}
