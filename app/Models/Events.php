<?php

namespace App\Models;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;  

class Events extends Model{ 

    protected $table = 'events'; 

    public static function valid(){ 
        if (!empty($_GET['q'])) {
            return self::where('title','LIKE','%'.$_GET['q'].'%') 
                       ->whereIn('status',[1])
                       ->paginate(10);
        } else {
            return self::whereIn('status',[1])
                       ->paginate(10);
        }     
    } 
    public static function archived(){ 
        if (!empty($_GET['q'])) {
            return self::where('title','LIKE','%'.$_GET['q'].'%') 
                       ->whereIn('status',[0])
                       ->paginate(10);
        } else {
            return self::whereIn('status',[0])
                       ->paginate(10);
        }     
    } 

} 
