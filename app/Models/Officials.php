<?php

namespace App\Models;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;  

class Officials extends Model{ 

    protected $table = 'officials'; 

    public static function valid(){ 
    	if (!empty($_GET['q'])) {
    		return self::where('position','LIKE','%'.$_GET['q'].'%') 
	        		   ->whereIn('status',[1])
	        		   ->paginate(10);
    	} else {
    		return self::whereIn('status',[1])
	       			   ->paginate(10);
    	}     
    } 

} 
