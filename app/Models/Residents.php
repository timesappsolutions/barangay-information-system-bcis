<?php

namespace App\Models;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;  

class Residents extends Model{ 

    protected $table = 'residents'; 

    public static function valid(){ 
        if (!empty($_GET['q'])) {
            return self::where('fname','LIKE','%'.$_GET['q'].'%')
                       ->orWhere('lname','LIKE','%'.$_GET['q'].'%')
                       ->orWhere('mname','LIKE','%'.$_GET['q'].'%')
                       ->whereIn('status',[1])
                       ->paginate(10);
        } else {
            return self::whereIn('status',[1])
                       ->paginate(10);
        }     
    } 
    public static function households(){ 
        
      return self::whereIn('position',['Head'])
                    ->whereIn('status',[1])
                    ->paginate(10); 
    } 
    public static function archived(){ 
        if (!empty($_GET['q'])) {
            return self::where('fname','LIKE','%'.$_GET['q'].'%')
                       ->orWhere('lname','LIKE','%'.$_GET['q'].'%')
                       ->orWhere('mname','LIKE','%'.$_GET['q'].'%')
                       ->whereIn('status',[0])
                       ->paginate(10);
        } else {
            return self::whereIn('status',[0])
                       ->paginate(10);
        }     
    } 

} 
