<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	
	Route::get('/dump' ,function(){
		try {
			DB::unprepared(file_get_contents('bcis.sql'));
			return "database imported!";
		} catch (Exception $e) {
			return $e;	
		}
	});

	Route::middleware(['guest'])->group(function(){ 

		// Un Authenticated
		Route::get         ('/',                           'Admin\Authentication@index'                       )->name('admin.login'); 
		Route::post        ('/authenticate',               'Admin\Authentication@auth'                        )->name('admin.login.authenticate'); 
 
	});

	Route::middleware(['admin'])->group(function(){
 
		// Dashboard
		Route::get         ('/dashboard',                  'Admin\Dashboard@index'                            )->name('admin.dashboard'); 

		// Logout
		Route::get         ('/logout',                     'Admin\Authentication@logout'                      )->name('admin.logout');  
 
		// Residents
		Route::prefix('residents')->group(function(){
			Route::get  ('/',                              'Admin\Residents@index'                            )->name('admin.residents');  
			Route::post ('/add',                           'Admin\Residents@add'                              )->name('admin.residents.add');  
			Route::post ('/update/{id}',                   'Admin\Residents@update'                           )->name('admin.residents.update');  
			Route::post ('/delete/{id}',                   'Admin\Residents@delete'                           )->name('admin.residents.delete');  
			Route::get  ('/print/certificate/{id}',        'Admin\Residents@cert'                             )->name('admin.residents.print.cert');  
			Route::get  ('/print/indigency/{id}',          'Admin\Residents@indi'                             )->name('admin.residents.print.indi');  
	
			Route::get  ('/households',                    'Admin\Residents@households'                       )->name('admin.households');  
		}); 

		// About
		Route::prefix('about')->group(function(){
			Route::get  ('/',                              'Admin\About@index'                                )->name('admin.about');    
		}); 
 
		// Events
		Route::prefix('events')->group(function(){
			Route::get  ('/',                              'Admin\Events@index'                               )->name('admin.events');  
			Route::post ('/add',                           'Admin\Events@add'                                 )->name('admin.events.add');  
			Route::post ('/update/{id}',                   'Admin\Events@update'                              )->name('admin.events.update');  
			Route::post ('/delete/{id}',                   'Admin\Events@delete'                              )->name('admin.events.delete');  
		}); 
 
		// Ordinances
		Route::prefix('ordinances')->group(function(){
			Route::get  ('/',                              'Admin\Ordinances@index'                           )->name('admin.ordinances');  
			Route::post ('/add',                           'Admin\Ordinances@add'                             )->name('admin.ordinances.add');  
			Route::post ('/update/{id}',                   'Admin\Ordinances@update'                          )->name('admin.ordinances.update');  
			Route::post ('/delete/{id}',                   'Admin\Ordinances@delete'                          )->name('admin.ordinances.delete');  
		}); 
 
		// Offenses
		Route::prefix('offenses')->group(function(){
			Route::get  ('/',                              'Admin\Offenses@index'                             )->name('admin.offenses');  
			Route::post ('/add',                           'Admin\Offenses@add'                               )->name('admin.offenses.add');  
			Route::post ('/update/{id}',                   'Admin\Offenses@update'                            )->name('admin.offenses.update');  
			Route::post ('/delete/{id}',                   'Admin\Offenses@delete'                            )->name('admin.offenses.delete');  
		}); 

		// Incidents
		Route::prefix('incidents')->group(function(){
			Route::get  ('/',                              'Admin\Incidents@index'                            )->name('admin.incidents');  
			Route::post ('/add',                           'Admin\Incidents@add'                              )->name('admin.incidents.add');  
			Route::post ('/update/{id}',                   'Admin\Incidents@update'                           )->name('admin.incidents.update');  
			Route::post ('/delete/{id}',                   'Admin\Incidents@delete'                           )->name('admin.incidents.delete');  
			Route::get  ('/print/certificate/{id}',        'Admin\Incidents@cert'                             )->name('admin.incidents.print.cert');   
		}); 
 
		// Officials
		Route::prefix('officials')->group(function(){
			Route::get  ('/',                              'Admin\Officials@index'                            )->name('admin.officials');  
			Route::post ('/add',                           'Admin\Officials@add'                              )->name('admin.officials.add');  
			Route::post ('/update/{id}',                   'Admin\Officials@update'                           )->name('admin.officials.update');  
			Route::post ('/delete/{id}',                   'Admin\Officials@delete'                           )->name('admin.officials.delete');  
		}); 

		// Archived
		Route::prefix('archived')->group(function(){
			Route::get  ('/residents',                     'Admin\Archived@residents'                         )->name('admin.archived.residents');    
			Route::post ('/residents/ret/{id}',            'Admin\Archived@residents_ret'                     )->name('admin.archived.residents.return');   
			Route::get  ('/events',                        'Admin\Archived@events'                            )->name('admin.archived.events');    
			Route::post ('/events/ret/{id}',               'Admin\Archived@events_ret'                        )->name('admin.archived.events.return');    
			Route::get  ('/ordinances',                    'Admin\Archived@ord'                               )->name('admin.archived.ordinances');    
			Route::post ('/ordinances/ret/{id}',           'Admin\Archived@ord_ret'                           )->name('admin.archived.ordinances.return');   
			Route::get  ('/incidents',                     'Admin\Archived@incidents'                         )->name('admin.archived.incidents');    
			Route::post ('/incidents/ret/{id}',            'Admin\Archived@incidents_ret'                     )->name('admin.archived.incidents.return');   
			Route::get  ('/offenses',                      'Admin\Archived@offenses'                          )->name('admin.archived.offenses');    
			Route::post ('/offenses/ret/{id}',             'Admin\Archived@offenses_ret'                      )->name('admin.archived.offenses.return');     
		}); 

		
		// Reports
		Route::prefix('reports')->group(function(){
			Route::get  ('/residents',                     'Admin\Reports@residents'                          )->name('admin.reports.residents');    
			Route::get  ('/indigent',                      'Admin\Reports@indigent'                           )->name('admin.reports.indigent');    
			Route::get  ('/events',                        'Admin\Reports@events'                             )->name('admin.reports.events');    
			Route::get  ('/ordinances',                    'Admin\Reports@ordinances'                         )->name('admin.reports.ordinances');    
			Route::get  ('/incidents',                     'Admin\Reports@incidents'                          )->name('admin.reports.incidents');    
			Route::get  ('/offences',                      'Admin\Reports@offences'                           )->name('admin.reports.offences');    
			Route::get  ('/officials',                     'Admin\Reports@officials'                          )->name('admin.reports.officials');    
			Route::get  ('/historical/records',            'Admin\Reports@hr'                                 )->name('admin.reports.hr');    
			Route::post ('/hr/print',                      'Admin\Reports@hr_print'                           )->name('admin.reports.hr.print');    
		}); 

	});





 