-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2018 at 01:59 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bcis`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `remember_token` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `image`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$OfSEVnrJjG7wlhbaRxDK8uNL423yLfzuQPymE.iw1Sjrhy36Pqe.i', 'logo.png', '', '1', '2018-09-23 23:39:42', '2018-09-23 23:39:42');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `date` varchar(225) NOT NULL,
  `remarks` varchar(999) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `date`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Meetings', '2018-10-25', 'All are required to attend.', '1', '2018-10-18 04:04:36', '2018-10-18 07:48:31'),
(2, 'General Assembly', '2018-10-19', 'At Stadium', '1', '2018-10-18 04:43:43', '2018-10-18 04:43:43'),
(3, 'test', '2018-11-18', 'xxx', '1', '2018-11-18 10:35:49', '2018-11-18 10:35:49');

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `id` int(11) NOT NULL,
  `type` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `date_happen` varchar(225) NOT NULL,
  `remarks` varchar(225) NOT NULL,
  `processed_by` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incidents`
--

INSERT INTO `incidents` (`id`, `type`, `location`, `date_happen`, `remarks`, `processed_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Purok 1', '', 'Caught 1:53 am', 'Nino Capalac', '1', '2018-10-18 05:23:58', '2018-10-18 07:56:09'),
(2, 'xxx', 'xxx', '2018-11-14', 'xxx', 'xxx', '0', '2018-11-18 10:44:09', '2018-11-18 10:46:36');

-- --------------------------------------------------------

--
-- Table structure for table `indigency`
--

CREATE TABLE `indigency` (
  `id` int(11) NOT NULL,
  `resident_id` varchar(225) NOT NULL,
  `purpose` varchar(225) NOT NULL,
  `prepared_by` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offenses`
--

CREATE TABLE `offenses` (
  `id` int(11) NOT NULL,
  `ordinance_id` varchar(255) NOT NULL,
  `resident_id` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `processed_by` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offenses`
--

INSERT INTO `offenses` (`id`, `ordinance_id`, `resident_id`, `location`, `remarks`, `processed_by`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', '2', 'Purok 2', 'Tanod Jail', 'Vicente Criste', '1', '2018-10-21 14:14:35', '2018-10-21 14:14:35'),
(2, '1', '1', 'xx', 'xx', 'xx', '1', '2018-11-18 10:47:50', '2018-11-18 10:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `officials`
--

CREATE TABLE `officials` (
  `id` int(11) NOT NULL,
  `resident_id` varchar(225) NOT NULL,
  `position` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officials`
--

INSERT INTO `officials` (`id`, `resident_id`, `position`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Captain', '0', '2018-10-18 05:45:44', '2018-10-20 09:08:44'),
(2, '2', 'Secretary', '0', '2018-10-20 08:59:49', '2018-10-20 09:07:01'),
(3, '3', 'Secretary', '1', '2018-10-20 09:07:11', '2018-10-20 09:07:11'),
(4, '4', 'Treasurer', '1', '2018-10-20 09:07:20', '2018-10-20 09:07:20'),
(5, '5', 'Kagawad', '1', '2018-10-20 09:07:36', '2018-10-20 09:07:36'),
(6, '6', 'Kagawad', '1', '2018-10-20 09:07:44', '2018-10-20 09:07:44'),
(7, '7', 'Captain', '1', '2018-10-20 09:08:51', '2018-10-20 09:08:51'),
(8, '1', 'Sk Chairman', '1', '2018-10-20 09:17:54', '2018-10-20 09:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `ordinances`
--

CREATE TABLE `ordinances` (
  `id` int(11) NOT NULL,
  `no` varchar(225) NOT NULL,
  `title` varchar(225) NOT NULL,
  `summary` varchar(225) NOT NULL,
  `approved` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordinances`
--

INSERT INTO `ordinances` (`id`, `no`, `title`, `summary`, `approved`, `status`, `created_at`, `updated_at`) VALUES
(1, '01289', 'Curfew', '10:00pm - 4:00am', 'Vicente Criste', '1', '2018-10-18 05:04:14', '2018-10-21 13:00:50'),
(2, '01234', 'Liquor Ban', '10:00pm - 5:00am', 'Vicente Criste', '1', '2018-10-21 14:12:57', '2018-10-21 14:12:57'),
(3, 'test', 'asd', 'asd', 'ad', '1', '2018-11-18 10:37:12', '2018-11-18 10:37:12');

-- --------------------------------------------------------

--
-- Table structure for table `residents`
--

CREATE TABLE `residents` (
  `id` int(11) NOT NULL,
  `fname` varchar(225) NOT NULL,
  `mname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `position` varchar(225) NOT NULL,
  `house_no` varchar(225) NOT NULL,
  `purok` varchar(225) NOT NULL,
  `birthplace` varchar(225) NOT NULL,
  `birthday` varchar(225) NOT NULL,
  `sex` varchar(225) NOT NULL,
  `civil_status` varchar(225) NOT NULL,
  `citizenship` varchar(225) NOT NULL,
  `occupation` varchar(225) NOT NULL,
  `indigency` varchar(225) NOT NULL DEFAULT '0',
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residents`
--

INSERT INTO `residents` (`id`, `fname`, `mname`, `lname`, `position`, `house_no`, `purok`, `birthplace`, `birthday`, `sex`, `civil_status`, `citizenship`, `occupation`, `indigency`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cresse Kenneth', 'Batan', 'Abenojar', 'Head', '061', '1', 'Kuwait', '1997-06-08', 'Male', 'Single', 'Filipino', 'Self Employed', '1', '1', '2018-10-18 03:36:20', '2018-10-18 07:42:41'),
(2, 'christian', 'b', 'fabie', 'Member', '061', '1', 'abra', '1996-07-03', 'Male', 'Single', 'filipino', 'student', '0', '1', '2018-10-20 07:36:54', '2018-10-20 07:36:54'),
(3, 'Reggie', 'V', 'Molina', 'Head', '002', '3', 'Vigan City', '1995-09-30', 'Female', 'Married', 'filipino', 'Brgy. Secretary', '0', '1', '2018-10-20 09:03:10', '2018-10-20 09:03:10'),
(4, 'Erlyn', 'A', 'Ramboyon', 'Member', '0003', '5', 'Vigan City', '1985-03-01', 'Female', 'Married', 'filipino', 'Brgy Treasurer', '0', '1', '2018-10-20 09:04:31', '2018-10-20 09:04:31'),
(5, 'Agustin', 'V', 'Aniceto', 'Head', '006', '5', 'Vigan City', '1986-12-31', 'Male', 'Married', 'filipino', 'Kagawad', '0', '1', '2018-10-20 09:05:27', '2018-10-20 09:05:27'),
(6, 'Jordan', 'A', 'Aslor', 'Head', '007', '3', 'Vigan City', '1990-07-26', 'Male', 'Married', 'filipino', 'Kagawad', '0', '1', '2018-10-20 09:06:24', '2018-10-20 09:06:24'),
(7, 'Vicente', 'A', 'Criste', 'Head', '0007', '5', 'Vigan City', '1974-11-30', 'Male', 'Married', 'filipino', 'Brgy Captain', '0', '1', '2018-10-20 09:08:32', '2018-10-20 09:08:32'),
(8, 'asd22222', 'asd', 'dsa', 'Head', '213', '1', 'asd', '2018-11-14', 'Male', 'Single', 'asd', 'asd', '0', '1', '2018-11-18 10:32:44', '2018-11-18 10:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `meta__id` varchar(225) NOT NULL,
  `meta_type` varchar(225) NOT NULL,
  `meta_key` varchar(225) NOT NULL,
  `meta_value` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `created_at` varchar(225) NOT NULL,
  `updated_at` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `meta__id`, `meta_type`, `meta_key`, `meta_value`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 'settings', 'vision', 'We envision \r\nBarangay Capangpangan \r\nto br progressive, clean, flood and drug free barangay \r\nwell disciplined, self-reliant and value oriented residents \r\nthrough participative and responsive \r\nbarangay government.', '1', '', ''),
(2, '', 'settings', 'mission', 'To fulfill our Vision, \r\nwe shall endeavor to enact and strictly implement tax ordinances, \r\nhold regular consultative meetings with barangay residents \r\nand conduct capability building program \r\nand education on health and s', '1', '', ''),
(3, '', 'settings', 'values', 'Team Work, Cooperation, Sharing, \r\nUnity Trust worthy, Commited, Leadership \r\nGood Governance, Honest\r\n\r\n', '1', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indigency`
--
ALTER TABLE `indigency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offenses`
--
ALTER TABLE `offenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officials`
--
ALTER TABLE `officials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordinances`
--
ALTER TABLE `ordinances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `residents`
--
ALTER TABLE `residents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `indigency`
--
ALTER TABLE `indigency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offenses`
--
ALTER TABLE `offenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `officials`
--
ALTER TABLE `officials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ordinances`
--
ALTER TABLE `ordinances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `residents`
--
ALTER TABLE `residents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
