@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">   
        <div class="col-lg-12">
            <div class="c-card c-overview-card u-p-medium u-mb-medium">
                <div class="row">
                    <div class="col-12 u-border-right">
                        <div class="c-overview-card__section">
                            <h3 class="u-mb-zero">Historical Records</h3>
                            <p class="u-text-mute u-mb-small">Choose Year</p>
                            <hr>
                            <br>
                            <div class="col-12">
                                 <form action="{{ URL::route('admin.reports.hr.print') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h5>Choose Year</h5>
                                            <select class="form-control" name="year">
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8"></div>
                                        <div class="col-md-4">
                                            <br>
                                            <h5>Choose Reports</h5>
                                            <input type="checkbox" name="generate[]" value="residents"> Residents <br>
                                            <input type="checkbox" name="generate[]" value="indigent"> Indigents <br>
                                            <input type="checkbox" name="generate[]" value="events"> Events <br>
                                            <input type="checkbox" name="generate[]" value="ordinances"> Ordinances <br>
                                            <input type="checkbox" name="generate[]" value="incidents"> Incidents <br>
                                            <input type="checkbox" name="generate[]" value="offences"> Offences <br> 
                                        </div> 
                                        <div class="col-md-12">
                                            <br><hr><br>
                                            <button class="c-btn c-btn--default pull-right">Create Report</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div> 
    </div> 
</div> 
@endsection 
