<div class="container">
	<div class="header">
		<div class="left">
			<img src="{{ URL::asset('uploads/seal.png') }}">
		</div>
		<div class="center">
			<div class="a">Republika ng Pilipinas</div>
			<div class="b">Lungsod ng Ilocos Sur</div>
			<div class="c">Barangay Capangpangan</div>
			<div class="d">Tangapan ng Punong Barangay</div>
		</div>
		<div class="right">
			<img src="{{ URL::asset('uploads/logo.png') }}">
		</div>
	</div>
	<div class="body">
		<div class="title"><b>Historical Records Report</b></div>
	</div>
	@foreach($data as $xxx => $yyy)
		@if(in_array($xxx,$generate))
		<div class="body"> 
			<div class="letter">
				<div class="intro">{{ strtoupper($yyy['title']) }} REPORTS FOR {{ $date[0] }} - {{ $date[1] }}</div> 
				<table cellpadding="0" cellspacing="0">
					@if($yyy['title'] === 'residents')
						<tr>
							@foreach($yyy['headers'] as $v)
								<th>{{ strtoupper(str_replace('_',' ',$v)) }}</th> 
							@endforeach
						</tr>
						@foreach($yyy['data'] as $d)
							<tr> 
								@foreach($yyy['headers'] as $v)
									@if($v == 'fullname')
										<th>{{ ucfirst($d->fname) }} {{ ucfirst($d->mname) }} {{ ucfirst($d->lname) }}</th> 
									@else
										<th>{{ ucfirst($d->$v) }}</th> 
									@endif
								@endforeach			
							</tr>
						@endforeach 
					@elseif($yyy['headers'])
						<tr>
							@foreach($yyy['headers'] as $v)
								<th>{{ strtoupper(str_replace('_',' ',$v)) }}</th> 
							@endforeach
						</tr>
						@foreach($yyy['data'] as $d)
							<tr> 
								@foreach($yyy['headers'] as $v)
									@if($v == 'fullname')
										<th>{{ ucfirst($d->fname) }} {{ ucfirst($d->mname) }} {{ ucfirst($d->lname) }}</th> 
									@else
										<th>{{ ucfirst($d->$v) }}</th> 
									@endif
								@endforeach			
							</tr>
						@endforeach 	
					@else
						@if($table == 'offenses')
							<tr>
								<th>ORDINANCE</th>
								<th>RESIDENT</th>
								<th>LOCATION</th>
								<th>REMARKS</th>
								<th>PROCESSED BY</th>
							</tr>
							@foreach($yyy['data'] as $d) 
								<tr> 
										<td>{{ App\Models\Ordinances::find($d->ordinance_id)->no }}. {{ ucfirst(App\Models\Ordinances::find($d->ordinance_id)->title) }}</td>
										<td>{{ ucfirst(App\Models\Residents::find($d->resident_id)->lname) }} {{ ucfirst(App\Models\Residents::find($d->resident_id)->fname) }}</td>
										<td>{{ ucfirst($d->location) }}</td>
										<td>{{ $d->remarks }}</td> 
										<td>{{ $d->processed_by }}</td> 
										<td>{{ ucfirst($d->processed_by) }}</td>  
								</tr>
							@endforeach 
						@else
							<tr>
								<th>FULLNAME</th>
								<th>POSITION</th>
								<th>UPDATED AT</th>
								<th>CREATED AT</th> 
							</tr>
							@foreach($yyy['data'] as $d) 
								<tr> 
									<td>{{ ucfirst(App\Models\Residents::find($d->resident_id)->lname) }} {{ ucfirst(App\Models\Residents::find($d->resident_id)->fname) }}</td>
									<td>{{ ucfirst($d->position) }}</td>
									<td>{{ $d->updated_at }}</td>
									<td>{{ $d->created_at }}</td> 				
								</tr>
							@endforeach 
						@endif
					@endif
				</table>
			</div>
		</div>
		@endif
	@endforeach
	<div class="body">
		<div class="sign">
			<b>{{ App\Models\Residents::find($captain->resident_id)->fname }} {{ App\Models\Residents::find($captain->resident_id)->mname }} {{ App\Models\Residents::find($captain->resident_id)->lname }}</b><br>
			<span>Brgy Captain</span>
		</div>
	</div>
</div>

<style type="text/css">
	.container .body .letter table{
		margin-top: 3%;
		width: 100%;
		float: left;
	}
	.container .body .letter table tr th{
		border: 1px solid #333;
		padding: 0.5%;
		text-align: left;
		font-size:10px;
	}
	.container .body .letter table tr td{
		border: 1px solid #333;
		padding: 0.5%;
		text-align: left;	
		font-size:10px;
	}
	.container .body .sign{
		margin-top: 10%;
		width: 100%;
		float: right;
		text-align: right;
	}
	.container .body .letter .main{ 
		width: 90%; 
		float: left;
		text-align: center;
		padding-left: 5%;
		padding-right: 5%;
		padding-top: 6%;
		font-style: italic;
		font-size: 17px;
		font-family: serif;
		line-height: 40px;
	}
	.container .body .letter .main .underlined{ 
		border-bottom: 1px solid #333; 
		padding-bottom: 1%;
		padding-left: 2%;
		padding-right: 2%;
	}
	.container .body .letter{ 
		margin-top: 5%;
		width: 90%;
		padding: 5%;
		float: left;
	}
	.container .body .letter .intro{
		width: 100%;
		float: left;
		font-style: normal;
		font-family: arial;
		font-size: 10px;
	}
	.container .body{
		width: 100%; 
		float: left;
	}
	.container .body .title{
		margin-top: 3%;
		width: 100%; 
		float: left;
		text-align: center;
		font-size: 15px;
		font-style: bolder;
		font-family: arial;
	}
	.container{
		width: 98%; 
		float: left;
		padding: 1%;
		background-image: url({{ URL::asset('public/uploads/printseal.png') }});
		background-size: 60% 100%;
		background-position:center;
		height:100vh;
		background-repeat:no-repeat;
	}
	.container .left{
		width: 30%; 
		float: left; 
	}
	.container .center{
		width: 40%; 
		float: left; 
	}
	.container .center .a{
		margin-top: 5%;
		font-size: 15px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .center .b{
		font-size: 13px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
		margin-top: 1%;
		margin-bottom: 2%;
	}
	.container .center .c{
		font-size: 20px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .center .d{
		font-size: 20px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .right{
		width: 30%; 
		float: right; 
	}
	.container .right img{
		margin-left: 20%;
		width: 50%;  
		float: left;
	}
	.container .left img{
		margin-right: 20%;
		width: 50%;  
		float: right;
	}
</style>
<script type="text/javascript">
	window.onload = function(){
		window.print();
	}
</script>