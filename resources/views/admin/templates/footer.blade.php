    <!-- Main javascsript --> 
    <script src="{{ URL::asset('theme/js/main.min3661.js?v=2.0') }}"></script> 
    <script src="{{ URL::asset('theme/js/jquery.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('plugins/calendar/lib/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('plugins/calendar/lib/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('plugins/calendar/fullcalendar.min.js') }}"></script>
	<script type="text/javascript">
		$('body .confirm-close').click(function(){
			if(window.confirm("Are you sure you want to close this form ?")){
				return true;
			}
			return false;
		});
	</script>
	<style type="text/css">
		.c-field{
			margin-bottom: 4%;
		}
		td,th{
			text-align: left !important;
		}
	</style>
</html>

@yield('extraJs')