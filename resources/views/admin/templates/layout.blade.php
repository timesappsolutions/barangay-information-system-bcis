@include('admin.templates.header')
<body class="o-page login-body"> 
	@include('admin.templates.menu') 
	<main class="o-page__content"> 
		@include('admin.templates.title')
		@yield('content') 
	</main>
</body>
@include('admin.templates.footer')
@include('admin.templates.notifications')  