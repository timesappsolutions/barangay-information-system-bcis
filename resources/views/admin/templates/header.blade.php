<!doctype html>
<html lang="en-us"> 
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>
            {{ $title }} | Brgy Capangpangan IS
        </title>

        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="{{ URL::asset('theme/css/fonts.css') }}" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="{{ URL::asset('theme/apple-touch-icon.png') }}">
        <link rel="shortcut icon" href="{{ URL::asset('uploads/logo.png') }}" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="{{ URL::asset('theme/css/main.min3661.css?v=2.0') }}">
        <link rel="stylesheet" href="{{ URL::asset('theme/css/custom.css') }}"> 
    </head>