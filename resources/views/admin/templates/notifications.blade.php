@if((session('success')))

	<div class="col-sm-6 col-lg-3 notifs notifs-success">	
		<div class="c-alert c-alert--success">
		    <i class="c-alert__icon fa fa-check-circle"></i> {{ session('success') }}
		</div>
	</div>

	<script>
		$('.notifs-success').fadeIn('slow');
		$('.notifs-success').click(function(){
			$('.notifs-success').slideUp();
		});
		setTimeout(function(){
			$('.notifs-success').slideUp();
		},3000);
	</script>

@endif


@if((session('error')))

	<div class="col-sm-6 col-lg-3 notifs notifs-error">	
		<div class="c-alert c-alert--danger">
		    <i class="c-alert__icon fa fa-exclamation-circle"></i> {{ session('error') }}
		</div>
	</div>

	<script>
		$('.notifs-error').fadeIn('slow');
		$('.notifs-error').click(function(){
			$('.notifs-error').slideUp();
		});
		setTimeout(function(){
			$('.notifs-error').slideUp();
		},3000);
	</script>

@endif