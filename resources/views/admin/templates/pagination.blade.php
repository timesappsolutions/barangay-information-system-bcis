@if(!empty($_GET['q']))
	@if($data->total() != 0)
	<nav class="c-pagination u-justify-center">
	    <ul class="c-pagination__list"> 
		        
	        @if($data->currentPage() != $data->onFirstPage())
	       		<li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ ($data->currentPage() - 1) }}&q={{ $_GET['q'] }}">
		                <i class="fa fa-caret-left"></i> 
		            </a>
		        </li>  
	        @else 
	        	<li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ $data->onFirstPage() }}&q={{ $_GET['q'] }}">
		                <i class="fa fa-caret-left"></i> 
		            </a>
		        </li>  
	        @endif

		    @for($i=1; $i <= $data->lastPage(); $i++)
	        	<li class="c-pagination__item">
	        		<a class="c-pagination__link @if($i == $data->currentPage())  is-active @endif" href="?page={{ $i }}&q={{ $_GET['q'] }}">
	        			{{ $i }}
	        		</a>
	        	</li>
	        @endfor  

	        @if($data->currentPage() != $data->lastPage())
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ ($data->currentPage() + 1) }}&q={{ $_GET['q'] }}">
		                <i class="fa fa-caret-right"></i>
		            </a>
		        </li> 
	        @else   
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ $data->lastPage() }}&q={{ $_GET['q'] }}">
		                <i class="fa fa-caret-right"></i>
		            </a>
		        </li> 
	        @endif
	    </ul>
	</nav>
	@endif
@else 
	@if($data->total() != 0)
	<nav class="c-pagination u-justify-center">
	    <ul class="c-pagination__list"> 

	        @if($data->currentPage() != $data->onFirstPage())
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ ( $data->currentPage() - 1 ) }}">
		                <i class="fa fa-caret-left"></i> 
		            </a>
		        </li>  
	        @else   
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ $data->onFirstPage() }}">
		                <i class="fa fa-caret-left"></i> 
		            </a>
		        </li>  
	        @endif


		    @for($i=1; $i <= $data->lastPage(); $i++)
	        	<li class="c-pagination__item">
	        		<a class="c-pagination__link @if($i == $data->currentPage())  is-active @endif" href="?page={{ $i }}">
	        			{{ $i }}
	        		</a>
	        	</li>
	        @endfor  

	        @if($data->currentPage() != $data->lastPage())
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ ($data->currentPage() + 1) }}">
		                <i class="fa fa-caret-right"></i>
		            </a>
		        </li> 
	        @else 
		        <li class="c-pagination__item">
		            <a class="c-pagination__control" href="?page={{ $data->lastPage() }}">
		                <i class="fa fa-caret-right"></i>
		            </a>
		        </li>   
	        @endif 
	    </ul>
	</nav>
	@endif
@endif