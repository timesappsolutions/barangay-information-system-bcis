<?php 
    $menu = array(
        array(
            'url'    => 'admin.dashboard', 
            'active' => 'dashboard', 
            'name'   => 'Dashboard', 
            'icon'   => 'line-chart', 
            'sub'    => false, 
        ),  
        array(
            'url'    => 'admin.residents', 
            'active' => 'residents', 
            'name'   => 'Residents', 
            'icon'   => 'user-o', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.households', 
            'active' => 'households', 
            'name'   => 'Household', 
            'icon'   => 'users', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.events', 
            'active' => 'events', 
            'name'   => 'Events', 
            'icon'   => 'calendar-o', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.ordinances', 
            'active' => 'ordinances', 
            'name'   => 'Ordinances', 
            'icon'   => 'align-left', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.incidents', 
            'active' => 'incidents', 
            'name'   => 'Incidents', 
            'icon'   => 'compass', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.offenses', 
            'active' => 'offenses', 
            'name'   => 'Offenses', 
            'icon'   => 'book', 
            'sub'    => false, 
        ),
        array(
            'url'    => 'admin.officials', 
            'active' => 'officials', 
            'name'   => 'Officials', 
            'icon'   => 'globe', 
            'sub'    => false, 
        ), 
        array(
            'url'    => 'admin.residents', 
            'active' => 'reports', 
            'name'   => 'Reports', 
            'icon'   => 'print', 
            'sub'    => true, 
            'data'   => array(
                array(
                    "name" => "Historical Records",
                    "url"  => "admin.reports.hr",
                    "open" => false
                ), 
                array(
                    "name" => "Print Residents",
                    "url"  => "admin.reports.residents",
                    "open" => true
                ), 
                array(
                    "name" => "Print Indigents",
                    "url"  => "admin.reports.indigent",
                    "open" => true
                ), 
                array(
                    "name" => "Print Events",
                    "url"  => "admin.reports.events",
                    "open" => true
                ),
                array(
                    "name" => "Print Ordinances",
                    "url"  => "admin.reports.ordinances",
                    "open" => true
                ),
                array(
                    "name" => "Print Incidents",
                    "url"  => "admin.reports.incidents",
                    "open" => true
                ),
                array(
                    "name" => "Print Officials",
                    "url"  => "admin.reports.officials",
                    "open" => true
                ) 
            )
        ),
        array(
            'url'    => 'admin.residents', 
            'active' => 'archived', 
            'name'   => 'Archived', 
            'icon'   => 'archive', 
            'sub'    => true, 
            'data'   => array(
                array(
                    "name" => "Residents",
                    "url" => "admin.archived.residents"
                ), 
                array(
                    "name" => "Events",
                    "url" => "admin.archived.events"
                ),
                array(
                    "name" => "Ordinances",
                    "url" => "admin.archived.ordinances"
                ),
                array(
                    "name" => "Incidents",
                    "url" => "admin.archived.incidents"
                ),
                array(
                    "name" => "Offenses",
                    "url" => "admin.archived.offenses"
                ) 
            )
        ),
        array(
            'url'    => 'admin.about', 
            'active' => 'about', 
            'name'   => 'About', 
            'icon'   => 'info-circle', 
            'sub'    => false, 
        ),  
    );
?>
<div class="o-page__sidebar js-page-sidebar">
    <div class="c-sidebar">
        <a class="c-sidebar__brand" href="#">
            Barangay Capangpangan Information System
        </a>
        
        <h4 class="c-sidebar__title">Dashboard</h4>
        <ul class="c-sidebar__list"> 

            <?php foreach ($menu as $k => $v) { ?>

                <?php if (!$v['sub']) { ?>

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link <?php if($active == $v['active']){ echo 'is-active'; } ?>" href="{{ URL::route($v['url']) }}">
                            <i class="fa fa-<?php echo $v['icon']; ?> u-mr-xsmall"></i><?php echo $v['name']; ?>
                        </a>
                    </li>    

                <?php } else { ?>

                    <li class="c-sidebar__item has-submenu <?php if($active == $v['active']){ echo 'is-active is-open'; } ?>">
                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_{{ $v['active'] }}" aria-expanded="false" aria-controls="sidebar-submenu">
                            <i class="fa fa-<?php echo $v['icon']; ?> u-mr-xsmall"></i><?php echo $v['name']; ?>
                        </a>
                        <ul class="c-sidebar__submenu collapse <?php if($active == $v['active']){ echo 'show'; } ?>" id="sidebar-submenu_{{ $v['active'] }}"> 

                            <?php foreach ($v['data'] as $x) { ?>

                                <li style="padding-left: 9%; "><a class="c-sidebar__link" href="{{ URL::route($x['url']) }}" @if(isset($x['open']) && $x['open']) target="_blank" @endif><?php echo $x['name']; ?></a></li> 

                            <?php } ?> 

                        </ul>
                    </li>

                <?php } ?> 

            <?php } ?>

        </ul>  
    </div>  
</div> 