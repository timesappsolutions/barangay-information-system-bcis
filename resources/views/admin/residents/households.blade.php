@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">  
		<div class="col-12">
		    <div class="c-table-responsive@desktop box"> 
		        <table class="c-table" id="datatable">
		            <caption class="c-table__title"> 
		                {{ $title }} <small>Database records</small>

			            <div class="col-sm-4 search-cont"> 
                            	<form method="get"> 
		                            <div class="c-field has-icon-right"> 
		                                <input class="c-input" type="text" placeholder="Search" name="q">
		                                <span class="c-field__icon">
		                                    <i class="fa fa-search"></i>
		                                </span>
		                            </div>
                            	</form>
	                        </div>
	                    </div>
		            </caption>

		            <thead class="c-table__head c-table__head--slim head-bg">
		                <tr class="c-table__row"> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text" width="1%"> Household No</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Fname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Mname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Lname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Position</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Sex</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Purok</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Updated At</th>   
		                    <th class="c-table__cell c-table__cell--head no-sorts center-text actionth">Action</th> 
		                </tr>
		            </thead>

		            <tbody>
		                @if($data && count($data) != 0) 
		                    @foreach($data as $key => $v)
		                        <tr class="c-table__row rowtable">    
	                                <td class="c-table__cell break">{{ $v->house_no }}</td>  
	                                <td class="c-table__cell break">{{ ucfirst($v->fname) }}</td>
	                                <td class="c-table__cell break">{{ ucfirst($v->mname) }}</td>
	                                <td class="c-table__cell break">{{ ucfirst($v->lname) }}</td> 
	                                <td class="c-table__cell break">{{ $v->position }}</td>  
	                                <td class="c-table__cell break">{{ $v->sex }}</td> 
	                                <td class="c-table__cell break">{{ $v->purok }}</td>   
	                                <td class="c-table__cell break">{{ $v->updated_at->diffForHumans() }}</td>   
		                            <td class="c-table__cell break last">   
        		                        <button class="c-btn c-btn--secondary" data-toggle="modal" data-target="#update_{{ $v->id }}">View</button>  
		                            </td>
		                        </tr>
		                    @endforeach 
		                @else
		                    <tr class="table__row">
		                        <td class="c-table__cell center-text" colspan="6">
		                            No records found on database !
		                        </td>
		                    </tr>
		                @endif
		            </tbody>
		        </table>
		        <br><br>
		        <div class="col-md-12 u-mb-medium">    
		        	@include('admin.templates.pagination', ['data' => $data ])
		        </div> 
		        <br>
		    </div> 
		</div>    
		

		<!-- Update -->
        @if($data && count($data) != 0) 
            @foreach($data as $key => $v)
				<div class="c-modal modal fade" id="update_{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="modal">
				    <div class="c-modal__dialog modal-dialog" role="document">
				    	<div class="modal-content">
				            <div class="c-card u-p-medium u-mh-auto modalmax">
				                <h3>All Members</h3> 
				  				
				                <table cellpadding="0" cellspacing="0">
				                	<tr>
				                		<th>Fname</th>
				                		<th>Mname</th>
				                		<th>Lname</th>
				                		<th>Sex</th>
				                	</tr>
				                	<?php 
				                		$members = App\Models\Residents::whereIn('house_no',[$v->house_no])
				                		->whereIn('purok',[$v->purok])
				                		->get();
				                	?>
				                	@if($members)
				                		@foreach($members as $x)
				                			<tr>
				                				<td>{{ ucfirst($x->fname) }}</td>
				                				<td>{{ ucfirst($x->mname) }}</td>
				                				<td>{{ ucfirst($x->lname) }}</td>
				                				<td>{{ $x->sex}}</td>
				                			</tr>
				                		@endforeach
				                	@else
				                		<center>
				                			No Members Found!
				                		</center>
				                	@endif
				                </table>
			                    <br>
				                <a class="confirm-close c-btn c-btn--danger btninline" data-dismiss="modal">
				                    Close
				                </a>
			            </div>
				        </div> 
				    </div>
				</div>
			@endforeach
		@endif

		<style type="text/css">
			table, tr{
				width: 100%;
			}
			tr td,tr th{
				border:0px solid lightgray;
			}
			tr, td{ 
				text-align: center;
			}
		</style>

    </div> 
</div> 
@endsection