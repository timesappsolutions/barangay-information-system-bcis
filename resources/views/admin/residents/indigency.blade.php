<div class="container">
	<div class="header">
		<div class="left">
			<img src="{{ URL::asset('uploads/seal.png') }}">
		</div>
		<div class="center">
			<div class="a">Republika ng Pilipinas</div>
			<div class="b">Lungsod ng Ilocos Sur</div>
			<div class="c">Barangay Capangpangan</div>
			<div class="d">Tangapan ng Punong Barangay</div>
		</div>
		<div class="right">
			<img src="{{ URL::asset('uploads/logo.png') }}">
		</div>
	</div>
	<div class="body">
		<div class="title"><b>CERTIFICATE OF INDIGENCY</b></div>
		<div class="letter">
			<div class="intro">To Whom It May Concern :</div>
			<div class="main">
				This is to certify that Mr./Mrs./Miss. <span class="underlined">
					{{ $data->lname }} {{ $data->fname }} {{ $data->mname }}
				</span> ,
				<span class="underlined">{{ substr($data->birthday, 0, 2) }} years of age</span> , <span class="underlined">{{ $data->civil }}</span> is a bonafide
				resident of this barangay, with postal address at <span class="underlined">#{{ $data->house_no }}. Purok {{ $data->purok }}</span>. The said person is of good moral character and an active member of the community. He/She is one of those who belong to a low-income family.
			</div>
			<div class="main">
				Given this <span class="underlined">{{ date('d') }}th</span> day of <span class="underlined">{{ date('m') }}</span> , <span class="underlined">{{ date('Y') }}</span>  at Barangay Capangpangan, City of Vigan, Ilocus Sur, Philippines 
			</div>
		</div>
		<div class="sign">
			<b>{{ strtoupper(App\Models\Residents::find($captain->resident_id)->fname) }} {{ strtoupper(App\Models\Residents::find($captain->resident_id)->mname) }} {{ strtoupper(App\Models\Residents::find($captain->resident_id)->lname) }}</b><br>
			<span>Brgy Captain</span>
		</div>
	</div>
</div>

<style type="text/css">
	.container .body .sign{
		margin-top: 10%;
		width: 100%;
		float: right;
		text-align: right;
	}
	.container .body .letter .main{ 
		width: 90%; 
		float: left;
		text-align: center;
		padding-left: 5%;
		padding-right: 5%;
		padding-top: 6%;
		font-style: italic;
		font-size: 17px;
		font-family: serif;
		line-height: 40px;
	}
	.container .body .letter .main .underlined{ 
		border-bottom: 1px solid #333; 
		padding-bottom: 1%;
		padding-left: 2%;
		padding-right: 2%;
	}
	.container .body .letter{ 
		margin-top: 5%;
		width: 90%;
		padding: 5%;
		float: left;
	}
	.container .body .letter .intro{
		width: 100%;
		float: left;
		font-style: normal;
		font-family: arial;
		font-size: 20px;
	}
	.container .body{
		width: 100%; 
		float: left;
	}
	.container .body .title{
		margin-top: 3%;
		width: 100%; 
		float: left;
		text-align: center;
		font-size: 30px;
		font-style: bolder;
		font-family: arial;
	}
	.container{
		width: 90%; 
		float: left;
		padding: 5%;
		background-image: url({{ URL::asset('public/uploads/printseal.png') }});
		background-size: 60% 100%;
		background-position:center;
		height:100vh;
		background-repeat:no-repeat;
	}
	.container .left{
		width: 30%; 
		float: left; 
	}
	.container .center{
		width: 40%; 
		float: left; 
	}
	.container .center .a{
		margin-top: 5%;
		font-size: 15px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .center .b{
		font-size: 13px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
		margin-top: 1%;
		margin-bottom: 2%;
	}
	.container .center .c{
		font-size: 20px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .center .d{
		font-size: 20px;
		font-family: serif;
		width: 100%;
		float: left;
		text-align: center;
	}
	.container .right{
		width: 30%; 
		float: right; 
	}
	.container .right img{
		margin-left: 20%;
		width: 50%;  
		float: left;
	}
	.container .left img{
		margin-right: 20%;
		width: 50%;  
		float: right;
	}
</style>
<script type="text/javascript">
	window.onload = function(){
		window.print();
	}
</script>