@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">  
       <div class="col u-mb-medium">
		    <a class="c-btn c-btn--default right" href="#!" data-toggle="modal" data-target="#modal">
		        <i class="fa fa-building u-mr-xsmall"></i>Add new
		    </a>
		</div>
		<div class="col-12">
		    <div class="c-table-responsive@desktop box"> 
		        <table class="c-table" id="datatable">
		            <caption class="c-table__title"> 
		                {{ $title }} <small>Database records</small>

			            <div class="col-sm-4 search-cont"> 
                            	<form method="get"> 
		                            <div class="c-field has-icon-right"> 
		                                <input class="c-input" type="text" placeholder="Search" name="q">
		                                <span class="c-field__icon">
		                                    <i class="fa fa-search"></i>
		                                </span>
		                            </div>
                            	</form>
	                        </div>
	                    </div>
		            </caption>

		            <thead class="c-table__head c-table__head--slim head-bg">
		                <tr class="c-table__row"> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text" width="1%"> Household No</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Fname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Mname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Lname</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Position</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Sex</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Purok</th>  
		                    <th class="c-table__cell c-table__cell--head no-sorts center-text actionth">Action</th> 
		                </tr>
		            </thead>

		            <tbody>
		                @if($data && count($data) != 0) 
		                    @foreach($data as $key => $v)
		                        <tr class="c-table__row rowtable">    
	                                <td class="c-table__cell break">{{ $v->house_no }}</td>  
	                                <td class="c-table__cell break">{{ ucfirst($v->fname) }}</td>
	                                <td class="c-table__cell break">{{ ucfirst($v->mname) }}</td>
	                                <td class="c-table__cell break">{{ ucfirst($v->lname) }}</td> 
	                                <td class="c-table__cell break">{{ $v->position }}</td>  
	                                <td class="c-table__cell break">{{ $v->sex }}</td> 
	                                <td class="c-table__cell break">{{ $v->purok }}</td>      
		                            <td class="c-table__cell break last">  
	        		                    <div class="c-dropdown dropdown"> 
	        		                        <button class="c-btn c-btn--secondary has-dropdown dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
	        		                        
	        		                        <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton"> 
	        		                            <a class="c-dropdown__item dropdown-item" data-toggle="modal" data-target="#view_{{ $v->id }}">
	        		                            	<i class="fa fa-eye u-mr-xsmall"></i> View
	        		                            </a>
	        		                            <a class="c-dropdown__item dropdown-item" data-toggle="modal" data-target="#update_{{ $v->id }}">
	        		                            	<i class="fa fa-pencil u-mr-xsmall"></i> Edit
	        		                            </a>
	        		                            <a class="c-dropdown__item dropdown-item" target="_blank" href="{{ URL::route('admin.residents.print.cert',[$v->id]) }}">
	        		                            	<i class="fa fa-print u-mr-xsmall"></i> Print Clearance
	        		                            </a>
	        		                            <a class="c-dropdown__item dropdown-item" target="_blank" href="{{ URL::route('admin.residents.print.indi',[$v->id]) }}">
	        		                            	<i class="fa fa-print u-mr-xsmall"></i> Print Indigency
	        		                            </a>
	        		                            <a class="c-dropdown__item dropdown-item" data-toggle="modal" data-target="#delete_{{ $v->id }}">
	        		                            	<i class="fa fa-trash-o u-mr-xsmall"></i> Archive
	        		                            </a>
	        		                        </div>
	        		                    </div> 
		                            </td>
		                        </tr>
		                    @endforeach 
		                @else
		                    <tr class="table__row">
		                        <td class="c-table__cell center-text" colspan="7">
		                            No records found on database !
		                        </td>
		                    </tr>
		                @endif
		            </tbody>
		        </table>
		        <br><br>
		        <div class="col-md-12 u-mb-medium">    
		        	@include('admin.templates.pagination', ['data' => $data ])
		        </div> 
		        <br>
		    </div> 
		</div>    
		<div class="c-modal modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
		    <div class="c-modal__dialog modal-dialog" role="document">
		    	<div class="modal-content">
		            <div class="c-card u-p-medium u-mh-auto modalmax"> 
		                <h3>Add New</h3> 
		  				<form action="{{ URL::route('admin.residents.add') }}" method="post" enctype="multipart/form-data"> 
		  					{{ csrf_field() }} 
			                         
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">First name *</label>
		                        <input class="c-input" id="input13" type="text" name="fname" required> 
		                    </div>  
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Middle name *</label>
		                        <input class="c-input" id="input13" type="text" name="mname" required> 
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Last name *</label>
		                        <input class="c-input" id="input13" type="text" name="lname" required> 
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Household No. *</label>
		                        <input class="c-input" id="input13" type="number" name="house_no" required> 
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Household Position</label>
		                        <select class="c-input" id="input13" name="position" required>
		                        	<option value="Head">Head</option>
		                        	<option value="Member">Member</option>
		                        </select>
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Purok</label>
		                        <select class="c-input" id="input13" name="purok" required>
		                        	<option value="1">1</option>
		                        	<option value="2">2</option>
		                        	<option value="3">3</option>
		                        	<option value="4">4</option>
		                        	<option value="5">5</option>
		                        	<option value="6">6</option>
		                        	<option value="7">7</option>
		                        	<option value="8">8</option>
		                        	<option value="9">9</option> 
		                        </select>
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Place of Birth *</label>
		                        <input class="c-input" id="input13" type="text" name="birthplace" required> 
		                    </div>  
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Date of Birth *</label>
		                        <input class="c-input" id="input13" type="date" name="birthday" required> 
		                    </div> 
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Gender</label>
		                        <select class="c-input" id="input13" name="sex" required>
		                        	<option value="Male">Male</option>
		                        	<option value="Female">Female</option> 
		                        </select>
		                    </div>  
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Civil Status</label>
		                        <select class="c-input" id="input13" name="civil_status" required>
		                        	<option value="Single">Single</option>
		                        	<option value="Married">Married</option> 
		                        	<option value="Widowed">Widowed</option>
		                        	<option value="Divorced">Divorced</option> 
		                        	<option value="Seperated">Seperated</option> 
		                        </select>
		                    </div>   
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Citizenship</label>
		                        <select class="c-input" id="input13" name="citizenship" required>
		                        	@foreach($countries as $v)
		                        		<option value="{{ $v->nationality }}" @if($v->num_code == 608) selected @endif>{{ $v->nationality }}</option> 
		                        	@endforeach
		                        </select>
		                    </div>  occupation
		                    <div class="c-field">
		                        <label class="c-field__label" for="input13">Occupation</label>
		                        <select class="c-input" id="input13" name="occupation" required>
		                        	@foreach($occupation as $v)
		                        		<option value="{{ ucfirst($v) }}">{{ ucfirst($v) }}</option> 
		                        	@endforeach
		                        </select>
		                    </div>   

		                    <br>
			                <button class="c-btn c-btn--info">
			                    Save
			                </button>
			                <a class="confirm-close c-btn c-btn--danger btninline" data-dismiss="modal">
			                    Cancel
			                </a>
		  				</form>
		            </div>
		        </div> 
		    </div>
		</div>

		<!-- Update -->
        @if($data && count($data) != 0) 
            @foreach($data as $key => $v)
				<div class="c-modal modal fade" id="update_{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="modal">
				    <div class="c-modal__dialog modal-dialog" role="document">
				    	<div class="modal-content">
				            <div class="c-card u-p-medium u-mh-auto modalmax">
				                <h3>Update</h3> 
				  				<form action="{{ URL::route('admin.residents.update',$v->id) }}" method="post" enctype="multipart/form-data"> 
				  					{{ csrf_field() }} 
					                         
				                    
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">First name *</label>
				                        <input class="c-input" id="input13" type="text" name="fname" value="{{ $v->fname }}" required> 
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Middle name *</label>
				                        <input class="c-input" id="input13" type="text" name="mname" value="{{ $v->mname }}" required> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Last name *</label>
				                        <input class="c-input" id="input13" type="text" name="lname" value="{{ $v->lname }}" required> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Household No. *</label>
				                        <input class="c-input" id="input13" type="number" name="house_no" value="{{ $v->house_no }}" required> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Household Position</label>
				                        <select class="c-input" id="input13" name="position" value="{{ $v->position }}" required>
				                        	<option value="Head">Head</option>
				                        	<option value="Member">Member</option>
				                        </select>
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Purok</label>
				                        <select class="c-input" id="input13" name="purok" required>
				                        	<option value="1">1</option>
				                        	<option value="2">2</option>
				                        	<option value="3">3</option>
				                        	<option value="4">4</option>
				                        	<option value="5">5</option>
				                        	<option value="6">6</option>
				                        	<option value="7">7</option>
				                        	<option value="8">8</option>
				                        	<option value="9">9</option> 
				                        </select>
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Place of Birth *</label>
				                        <input class="c-input" id="input13" type="text" name="birthplace" value="{{ $v->birthplace }}" required> 
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Date of Birth *</label>
				                        <input class="c-input" id="input13" type="date" name="birthday" value="{{ $v->birthday }}" required> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Gender</label>
				                        <select class="c-input" id="input13" name="sex" required>
				                        	<option value="Male">Male</option>
				                        	<option value="Female">Female</option> 
				                        </select>
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Civil Status</label>
				                        <select class="c-input" id="input13" name="civil_status" required> 
				                        	<option value="Single"    @if($v->civil_status == 'Single') selected @endif>Single</option>
				                        	<option value="Married"   @if($v->civil_status == 'Married') selected @endif>Married</option> 
				                        	<option value="Widowed"   @if($v->civil_status == 'Widowed') selected @endif>Widowed</option>
				                        	<option value="Divorced"  @if($v->civil_status == 'Divorced') selected @endif>Divorced</option> 
				                        	<option value="Seperated" @if($v->civil_status == 'Seperated') selected @endif>Seperated</option> 
				                        </select>
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Citizenship *</label>
				                        <input class="c-input" id="input13" type="text" name="citizenship" value="{{ $v->citizenship }}" required> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Occupation *</label>
				                        <input class="c-input" id="input13" type="text" name="occupation" value="{{ $v->occupation }}" required> 
				                    </div> 

				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Citizenship</label>
				                        <select class="c-input" id="input13" name="citizenship" required>
				                        	@foreach($countries as $x)
				                        		<option value="{{ $x->nationality }}" @if($x->nationality == $v->citizenship) selected @endif>{{ $x->nationality }}</option> 
				                        	@endforeach
				                        </select>
				                    </div>  occupation
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Occupation</label>
				                        <select class="c-input" id="input13" name="occupation" required>
				                        	@foreach($occupation as $x)
				                        		<option value="{{ ucfirst($x) }}" @if(ucfirst($x) == $v->occupation) selected @endif>{{ ucfirst($x) }}</option> 
				                        	@endforeach
				                        </select>
				                    </div>  

				                    <br>
					                <button class="c-btn c-btn--info">
					                    Update
					                </button>
					                <a class="c-btn c-btn--danger btninline" data-dismiss="modal">
					                    Cancel
					                </a>
				  				</form>
				            </div>
				        </div> 
				    </div>
				</div>

				<div class="c-modal modal fade" id="view_{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="modal">
				    <div class="c-modal__dialog modal-dialog" role="document">
				    	<div class="modal-content">
				            <div class="c-card u-p-medium u-mh-auto modalmax">
				                <h3>Details</h3> 
				  				<form action="" method="post" enctype="multipart/form-data"> 
				  					{{ csrf_field() }} 
					                         
				                    
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">First name</label>
				                        <input class="RO c-input" id="input13" type="text" name="fname" value="{{ ucfirst($v->fname) }}" readonly> 
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Middle name</label>
				                        <input class="RO c-input" id="input13" type="text" name="mname" value="{{ ucfirst($v->mname) }}" readonly> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Last name</label>
				                        <input class="RO c-input" id="input13" type="text" name="lname" value="{{ ucfirst($v->lname) }}" readonly> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Household No.</label>
				                        <input class="RO c-input" id="input13" type="number" name="house_no" value="{{ $v->house_no }}" readonly> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Household Position</label>
				                        <select class="RO c-input" id="input13" name="position" value="{{ $v->position }}" readonly>
				                        	<option value="Head">Head</option>
				                        	<option value="Member">Member</option>
				                        </select>
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Purok</label>
				                        <select class="RO c-input" id="input13" name="purok" readonly>
				                        	<option value="1">1</option>
				                        	<option value="2">2</option>
				                        	<option value="3">3</option>
				                        	<option value="4">4</option>
				                        	<option value="5">5</option>
				                        	<option value="6">6</option>
				                        	<option value="7">7</option>
				                        	<option value="8">8</option>
				                        	<option value="9">9</option> 
				                        </select>
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Place of Birth</label>
				                        <input class="RO c-input" id="input13" type="text" name="birthplace" value="{{ $v->birthplace }}" readonly> 
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Date of Birth</label>
				                        <input class="RO c-input" id="input13" type="date" name="birthday" value="{{ $v->birthday }}" readonly> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Gender</label>
				                        <select class="RO c-input" id="input13" name="sex" readonly>
				                        	<option value="Male">Male</option>
				                        	<option value="Female">Female</option> 
				                        </select>
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Civil Status</label>
				                        <select class="RO c-input" id="input13" name="civil_status" readonly> 
				                        	<option value="Single"    @if($v->civil_status == 'Single') selected @endif>Single</option>
				                        	<option value="Married"   @if($v->civil_status == 'Married') selected @endif>Married</option> 
				                        	<option value="Widowed"   @if($v->civil_status == 'Widowed') selected @endif>Widowed</option>
				                        	<option value="Divorced"  @if($v->civil_status == 'Divorced') selected @endif>Divorced</option> 
				                        	<option value="Seperated" @if($v->civil_status == 'Seperated') selected @endif>Seperated</option> 
				                        </select>
				                    </div>  
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Citizenship</label>
				                        <input class="RO c-input" id="input13" type="text" name="citizenship" value="{{ $v->citizenship }}" readonly> 
				                    </div> 
				                    <div class="c-field">
				                        <label class="c-field__label" for="input13">Occupation</label>
				                        <input class="RO c-input" id="input13" type="text" name="occupation" value="{{ $v->occupation }}" readonly> 
				                    </div> 

				                    <br> 
					                <a class="confirm-close c-btn c-btn--danger btninline" data-dismiss="modal">
					                    Close
					                </a>
				  				</form>
				            </div>
				        </div> 
				    </div>
				</div>

				<div class="c-modal modal fade" id="delete_{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="modal">
				    <div class="c-modal__dialog modal-dialog" role="document">
				    	<div class="modal-content">
				            <div class="c-card u-p-medium u-mh-auto modalmax">
				                <h3>Confirm action</h3> 
				  				<form action="{{ URL::route('admin.residents.delete',$v->id) }}" method="post" enctype="multipart/form-data"> 
				  					{{ csrf_field() }} 
					                         
				                    <center>Are you sure you want to <b>Archive</b> this ?</center>

				                    <br>
					                <button class="c-btn c-btn--info">
					                    Proceed
					                </button>
					                <a class="confirm-close c-btn c-btn--danger btninline" data-dismiss="modal">
					                    Cancel
					                </a>
				  				</form>
				            </div>
				        </div> 
				    </div>
				</div>
			@endforeach
		@endif

    </div> 
</div>  
<style type="text/css">
	.RO{
		border:none !important;
		border-bottom:1px solid lightgray !important;
	}
</style>
@endsection