@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">  
        <div class="col-sm-12 col-lg-12">
            <h3 class="u-mb-small">Dashboard Overview</h3> 
        </div>
        <div class="col-sm-4 col-lg-4">
            <div class="c-state  c-state--success"> 
                <h4 class="c-state__number">{{ $data['residents'] }}</h4>
                <p class="c-state__status">
                    <a href="{{ URL::route('admin.residents') }}">Residents</a>
                </p>
                <span class="c-state__indicator">
                    <i class="fa fa-users"></i>
                </span>
            </div> 
        </div>  
        <div class="col-sm-4 col-lg-4">
            <div class="c-state  c-state--success"> 
                <h4 class="c-state__number">{{ $data['households'] }}</h4>
                <p class="c-state__status">
                    <a href="{{ URL::route('admin.households') }}">Households</a>
                </p>
                <span class="c-state__indicator">
                    <i class="fa fa-users"></i>
                </span>
            </div> 
        </div>  
        <div class="col-sm-4 col-lg-4">
            <div class="c-state  c-state--success"> 
                <h4 class="c-state__number">{{ $data['senior'] }}</h4>
                <p class="c-state__status">
                    <a href="{{ URL::route('admin.residents') }}">Senior Citizen</a>
                </p>
                <span class="c-state__indicator">
                    <i class="fa fa-user"></i>
                </span>
            </div> 
        </div>   

        <div class="col-lg-12">
            <div class="c-card c-overview-card u-p-medium u-mb-medium">
                <div class="row">
                    <div class="col-12 u-border-right">
                        <div class="c-overview-card__section">
                            <h3 class="u-mb-zero">{{ $data['residents'] }}</h3>
                            <p class="u-text-mute u-mb-small">Barangay Population</p>
                            <div id="container" style="width: 100%;">
                                <canvas id="canvas"></canvas>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-12" id="calendar">
            <div class="c-card c-overview-card u-p-medium u-mb-medium">
                <div class="row">
                    <div class="col-12">
                        <h3>Calendar of Events</h3><br>
                        <div class="js-xcalendar"></div>
                    </div> 
                </div>
            </div>
        </div>
    </div> 
</div> 
@endsection
@section('extraJs')
<script type="text/javascript" src="{{ URL::asset('plugins/charts/dist/Chart.bundle.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('plugins/charts/samples/utils.js') }}"></script> 
<script> 
    var color = Chart.helpers.color;
    var barChartData = {
        labels: <?php echo json_encode($graph['purok']); ?>,
        datasets: [{
            label: 'Residents',
            backgroundColor:  [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor:  [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderWidth: 1,
            data: <?php echo json_encode($graph['counts']); ?>
        }]

    };

    window.onload = function() {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
                title: {
                    display: true,
                    text: 'Population Graph'
                }
            }
        });

    };

    $(document).ready(function() {
        $(".js-xcalendar").fullCalendar({
            header: {
                left:"prev,next",
                center:"title",
                right:"month,agendaWeek,agendaDay"
            },
            dayNamesShort:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
            timeFormat:"hh:mm a",
            events: <?php echo $events; ?>
        });
    });
</script> 
@endsection
