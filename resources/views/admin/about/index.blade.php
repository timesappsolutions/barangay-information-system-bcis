@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">   
    	<div class="col-12">
    		<div class="c-table-responsive@desktop box">
    			<link rel="stylesheet" href="{{ URL::asset('plugins/treant-js-master/treant-js-master/Treant.css') }}">
			    <link rel="stylesheet" href="{{ URL::asset('plugins/treant-js-master/treant-js-master/basic-example.css') }}">
			    <div class="chart" id="basic-example"></div>
			    <script src="{{ URL::asset('plugins/treant-js-master/treant-js-master/vendor/raphael.js') }}"></script>
			    <script src="{{ URL::asset('plugins/treant-js-master/treant-js-master/Treant.js') }}"></script>
			    <div id="OrganiseChart6" style="height:50vw;"></div>

			    <br><br><br>
     		</div>
     	</div>
	    <script>
	          var tree_structure = {
                    chart: {
                        container: "#OrganiseChart6", 
                        levelSeparation:    70,
                        siblingSeparation: 300,
                        subTeeSeparation:   20,
                        rootOrientation: "NORTH", 
                        nodeAlign : 'CENTER',
    
                        node: {
                            HTMLclass: "tennis-draw",
                            drawLineThrough: true
                        },
                        connectors: {
                            type: "step",
                            style: {
                                "stroke-width": 3,
                                "stroke": "#f1f1f1"
                            }
                        } 
                    },
                    
                    nodeStructure: {
                        HTMLid:"parents",
                        image : "{{ URL::asset('uploads/user.jpg') }}" ,
                        text: { 
                            name: "{{ App\Models\Residents::find($official['captain']->resident_id )->fname }} {{ App\Models\Residents::find($official['captain']->resident_id )->mname }} {{ App\Models\Residents::find($official['captain']->resident_id )->lname }}",
                            desc: "Barangay Captain"
                        }, 
                        children: [
                            {
                        		image : "{{ URL::asset('uploads/user.jpg') }}" ,
                                HTMLid:"parents",
                                text:{
                                	name: "{{ @App\Models\Residents::find($official['secretary']->resident_id )->fname }} {{ @App\Models\Residents::find($official['secretary']->resident_id )->mname }} {{ @App\Models\Residents::find($official['secretary']->resident_id )->lname }}",
                         			desc: "Secretary", 
                                },
                            },
                            {
                        		image : "{{ URL::asset('uploads/user.jpg') }}" ,
                                HTMLid:"parents",
                                text:{ 
                                	title:"Barangay Officials",
                                	desc:"" 
                                },
                                children: [
                                	@foreach($official['sub'] as $v)
                                	{
		                        		image : "{{ URL::asset('uploads/user.jpg') }}" ,
		                                HTMLid:"parents",
		                                text:{
		                                	name: "{{ @App\Models\Residents::find($v->resident_id )->fname }} {{ @App\Models\Residents::find($v->resident_id )->mname }} {{ @App\Models\Residents::find($v->resident_id )->lname }}",
		                         			desc: "{{ $v->position }}", 
		                                },
		                            },
		                            @endforeach
                                ]
                            },
                            {
                        		image : "{{ URL::asset('uploads/user.jpg') }}" ,
                                HTMLid:"parents",
                                text:{
                                	name: "{{ @App\Models\Residents::find($official['treasurer']->resident_id )->fname }} {{ @App\Models\Residents::find($official['treasurer']->resident_id )->mname }} {{ @App\Models\Residents::find($official['treasurer']->resident_id )->lname }}",
                         			desc:"Treasurer" 
                                },
                            }
                        ]
                    }
                }


            new Treant( tree_structure ); 

	    </script>
    		</div>
    	</div>
		<div class="col-12">
			<br><br>
		    <div class="c-table-responsive@desktop box"> 
		        <div class="container-fluid">
		        	<center>
		        		<br><br>
		        		<img src="{{ URL::asset('uploads/bg-light.png') }}" width="20%">
		        	</center>
		        	<br>
		        	<div class="col-12">
		        		<h4>Mission</h4>
		        		<p>{{ $data[0]->meta_value }}</p>
		        	</div>
		        	<br>
		        	<div class="col-12">
		        		<h4>Vision</h4>
		        		<p>{{ $data[1]->meta_value }}</p>
		        	</div>
		        	<br>
		        	<div class="col-12">
		        		<h4>Core Values</h4>
		        		<p>{{ $data[2]->meta_value }}</p>
		        	</div>
		        </div>
		        <br><br>  
		    </div> 
		</div>     
    </div> 
</div> 
@endsection