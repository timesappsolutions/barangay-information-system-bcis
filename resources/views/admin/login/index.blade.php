@section('title','Login')
@include('admin.templates.header') 
    <body class="o-page o-page--center login-body custom-bg"> 
        <div class="o-page__card">
            <div class="c-card u-mb-xsmall">
                <header class="c-card__header u-pt-large">
                    <a class="c-card__icon" href="#!">
                        <img src="{{ URL::asset('uploads/logo.png') }}" alt="Dashboard UI Kit">
                    </a>
                    <h1 class="u-h3 u-text-center u-mb-zero">Barangay Capangpangan IS</h1>
                </header>
                
                <form class="c-card__body" method="post" action="{{ URL::route('admin.login.authenticate') }}">
                    {{ csrf_field() }}

                    @if(session('error'))
                        <p style="color:red;">{{ session('error') }}</p>
                    @endif 
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input1">Username</label> 
                        <input class="c-input" type="text" id="input1" name="username" required> 
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input2">Password</label> 
                        <input class="c-input" type="password" id="input2" name="password" required> 
                    </div>

                    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Sign In</button> 
                </form>
            </div>

            <div class="o-line">
                <a class="u-text-mute u-text-small">BCIS</a> 
                <a class="u-text-mute u-text-small">&copy 2018</a> 
            </div>
        </div>
    </body> 
    <style type="text/css">
        .custom-bg{
            background-size: 100% 100%; 
            background-attachment: fixed;
            background-image: url({{ URL::asset('uploads/bg.png') }});
            background-repeat: no-repeat;
        }
    </style>
@include('admin.templates.footer')