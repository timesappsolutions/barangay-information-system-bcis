@extends('admin.templates.layout') 
@section('content')
<div class="container-fluid">
    <div class="row">   
		<div class="col-12">
		    <div class="c-table-responsive@desktop box"> 
		        <table class="c-table" id="datatable">
		            <caption class="c-table__title"> 
		                {{ $title }} <small>Database records</small>

			            <div class="col-sm-4 search-cont"> 
                            	<form method="get"> 
		                            <div class="c-field has-icon-right"> 
		                                <input class="c-input" type="text" placeholder="Search" name="q">
		                                <span class="c-field__icon">
		                                    <i class="fa fa-search"></i>
		                                </span>
		                            </div>
                            	</form>
	                        </div>
	                    </div>
		            </caption>

		            <thead class="c-table__head c-table__head--slim head-bg">
		                <tr class="c-table__row"> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text" width="1%">Type</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Location</th>  
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Summary</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Date happen</th> 
	                        <th class="c-table__cell c-table__cell--head no-sorts center-text">Processed by</th>  
		                    <th class="c-table__cell c-table__cell--head no-sorts center-text actionth">Action</th> 
		                </tr>
		            </thead>

		            <tbody>
		                @if($data && count($data) != 0) 
		                    @foreach($data as $key => $v)
		                        <tr class="c-table__row rowtable">    
	                                <td class="c-table__cell break">{{ $v->type }}</td>   
	                                <td class="c-table__cell break">{{ $v->location }}</td>  
	                                <td class="c-table__cell break">{{ str_limit($v->remarks,50) }}</td> 
	                                <td class="c-table__cell break">{{ $v->date_happen }}</td>    
	                                <td class="c-table__cell break">{{ $v->processed_by }}</td>    
		                            <td class="c-table__cell break last">  
	        		                    <div class="c-dropdown dropdown"> 
	        		                        <button class="c-btn c-btn--secondary has-dropdown dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
	        		                        
	        		                        <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton">  
	        		                            <a class="c-dropdown__item dropdown-item" data-toggle="modal" data-target="#delete_{{ $v->id }}">
	        		                            	<i class="fa fa-trash-o u-mr-xsmall"></i> Retrieve
	        		                            </a>
	        		                        </div>
	        		                    </div> 
		                            </td>
		                        </tr>
		                    @endforeach 
		                @else
		                    <tr class="table__row">
		                        <td class="c-table__cell center-text" colspan="6">
		                            No records found on database !
		                        </td>
		                    </tr>
		                @endif
		            </tbody>
		        </table>
		        <br><br>
		        <div class="col-md-12 u-mb-medium">    
		        	@include('admin.templates.pagination', ['data' => $data ])
		        </div> 
		        <br>
		    </div> 
		</div>     
		<!-- Update -->
        @if($data && count($data) != 0) 
            @foreach($data as $key => $v) 
				<div class="c-modal modal fade" id="delete_{{ $v->id }}" tabindex="-1" role="dialog" aria-labelledby="modal">
				    <div class="c-modal__dialog modal-dialog" role="document">
				    	<div class="modal-content">
				            <div class="c-card u-p-medium u-mh-auto modalmax">
				                <h3>Confirm action</h3> 
				  				<form action="{{ URL::route('admin.archived.incidents.return',$v->id) }}" method="post" enctype="multipart/form-data"> 
				  					{{ csrf_field() }} 
					                         
				                    <center>Are you sure you want to <b>retrieve</b> this ?</center>

				                    <br>
					                <button class="c-btn c-btn--info">
					                    Proceed
					                </button>
					                <a class="c-btn c-btn--danger btninline" data-dismiss="modal">
					                    Cancel
					                </a>
				  				</form>
				            </div>
				        </div> 
				    </div>
				</div>
			@endforeach
		@endif

    </div> 
</div> 
@endsection